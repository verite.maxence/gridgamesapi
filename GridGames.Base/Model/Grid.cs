﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GridGames.Base.Model
{
    public class Grid
    {
        private int[] Value { get; set; }

        public Grid(int x, int y)
        {
            this.Value = new int[x * y];
        }

        public int[] Get()
        {
            return this.Value;
        }

        public int GetCell(int x, int y) {
            return this.Value[x * y];
        }

        public int[] SetCell(int value, int x, int y)
        {
            this.Value[x * y] = value;
            return this.Value;
        }

       
    }
}
