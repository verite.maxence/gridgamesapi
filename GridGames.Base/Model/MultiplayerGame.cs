﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace GridGames.Base.Model
{
    public abstract class MultiplayerGame : Game
    {

        public MultiplayerGame(int nbPlayers, bool randomStart) : base()
        {

            var firstPlayer = 0;
            this.NumberOfPlayers = nbPlayers;

            if (randomStart)
            {
                Random rand = new Random();
                firstPlayer = rand.Next(nbPlayers);
            }
            this.CurrentPlayer = firstPlayer;

        }

        public int CurrentPlayer { get; private set; }

        public int NumberOfPlayers { get; private set; }
     


        public int NextTurn()
        {
            if (this.CurrentPlayer == NumberOfPlayers)
            {
                this.CurrentPlayer = 1;
            }
            else this.CurrentPlayer++;

            return this.CurrentPlayer;
        }
    }
}
