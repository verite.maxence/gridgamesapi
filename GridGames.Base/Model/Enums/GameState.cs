﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GridGames.Base.Model.Enums
{
    public enum GameState
    {
        NOT_STARTED, PLAYING, OVER
    }
}
