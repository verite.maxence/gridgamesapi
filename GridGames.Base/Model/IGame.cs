﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using System.Text;

namespace GridGames.Base.Model
{
    public interface IGame
    {
        void Start();
        int Play(int x, int y, int? value);
        void End();

    }
}
