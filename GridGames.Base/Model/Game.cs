﻿using GridGames.Base.Model.Enums;
using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using System.Text;

namespace GridGames.Base.Model
{
    public abstract class Game : IGame
    {
        public Game()
        {
            this.State = GameState.NOT_STARTED;
        }

        public Grid Grid { get; protected set; }
        public GameState State { get; protected set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }

        public TimeSpan GlobalTime { get { return DateTime.Now - this.StartTime; } }

        public abstract void End();
        public abstract int Play(int x, int y, int? value);
        public abstract void Start();
    }
}
