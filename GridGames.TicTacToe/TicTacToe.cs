﻿using GridGames.Base.Model;
using System;
using System.Transactions;

namespace GridGames.TicTacToe
{
    /// <summary>
    /// Tic Tac Toe player wins when a row, col or dial is full of their respectives symbols
    /// 
    /// in this alg, i decided, as i've seen on some discuss, to test win conditions based on positive or negative points 
    /// for each columns, rows, and both diagonals.
    /// 
    /// Ex : If players that has "negative" win reach -3 in a line, he wins. If he negates something positive,
    /// that line can't be win by both of the players, so she's put to 5 (that lane is "done")
    /// 
    /// This is a solution that's harder to read, but O(1) complexity (quicker)
    /// </summary>

    public class TicTacToe : MultiplayerGame
    {

        /// <summary>
        /// Helps to calculate win condition
        /// </summary>
        public int[] RowsPoint { get; private set; }
        public int[] ColsPoint { get; private set; }
        public int DiagPoint { get; private set; }
        public int AntiDiagPoint { get; private set; }

        public TicTacToe() : base(2, true)
        {
            this.Grid = new Grid(3, 3);
            this.RowsPoint = new int[] { 0, 0, 0 };
            this.ColsPoint = new int[] { 0, 0, 0 };

            this.DiagPoint = 0;
            this.AntiDiagPoint = 0;
        }

        public override int Play(int x, int y, int? value)
        {

            if (this.State == Base.Model.Enums.GameState.PLAYING)
            {
                if (this.Grid.GetCell(x, y) == (int)GridValue.EMPTY)
                {
                    if (this.CurrentPlayer == (int)GridValue.PLAYER_ONE)
                    {
                        this.Grid.SetCell((int)GridValue.PLAYER_ONE, x, y);

                        // rows
                        if (this.RowsPoint[x] != 5)
                        {

                            if (this.RowsPoint[x] < 0) { this.RowsPoint[x] = 5; }
                            else
                            {
                                this.RowsPoint[x]++;

                                if (this.RowsPoint[x] == 3) {
                                    End();
                                    return (int)GridValue.PLAYER_ONE; }
                            }

                        }

                        //cols
                        if (this.ColsPoint[x] != 5)
                        {
                            if (this.ColsPoint[y] < 0) { this.ColsPoint[y] = 5; }
                            else
                            {
                                this.ColsPoint[y]++;

                                if (this.ColsPoint[y] == 3) {
                                    End(); 
                                    return (int)GridValue.PLAYER_ONE; }
                            }
                        }


                        //diag

                        if (x == y)
                        {
                            if (x == 1 && this.AntiDiagPoint != 5) // middle cell
                            {
                                if (this.AntiDiagPoint < 0) { this.AntiDiagPoint = 5; }
                                else
                                {
                                    this.AntiDiagPoint++;

                                    if (this.AntiDiagPoint == 3) {
                                        End();
                                        return (int)GridValue.PLAYER_ONE; }
                                }
                            }

                            if (this.DiagPoint != 5)
                            {
                                if (this.DiagPoint > 0)
                                {
                                    this.DiagPoint = 5;
                                }
                                else
                                {
                                    this.DiagPoint++;

                                    if (this.DiagPoint == 3) {
                                        End(); 
                                        return (int)GridValue.PLAYER_ONE; }
                                }
                            }
                        }

                        //antidiag
                        if (x + y == 2)
                        {
                            if (this.AntiDiagPoint >= 0 && this.AntiDiagPoint != 5)
                            {
                                this.AntiDiagPoint++;
                                if (this.AntiDiagPoint == 3) {
                                    End();
                                    return (int)GridValue.PLAYER_ONE; }
                            }
                            else
                            {
                                this.AntiDiagPoint = 5;
                            }
                        }
                    }
                    else
                    {

                        this.Grid.SetCell((int)GridValue.PLAYER_TWO, x, y);

                        // rows
                        if (this.RowsPoint[x] != 5)
                        {
                            if (this.RowsPoint[x] > 0) { this.RowsPoint[x] = 5; }
                            else
                            {
                                this.RowsPoint[x]--;

                                if (this.RowsPoint[x] == -3) {
                                    End();
                                    return (int)GridValue.PLAYER_TWO; }
                            }
                        }
                        //cols
                        if (this.ColsPoint[x] != 5)
                        {
                            if (this.ColsPoint[y] > 0) { this.ColsPoint[y] = 5; }
                            else
                            {
                                this.ColsPoint[y]--;

                                if (this.ColsPoint[y] == -3) {
                                    End();
                                    return (int)GridValue.PLAYER_TWO; }
                            }
                        }


                        //diag

                        if (x == y)
                        {
                            if (x == 1 && this.AntiDiagPoint != 5) // middle cell
                            {
                                if (this.AntiDiagPoint > 0) { this.AntiDiagPoint = 5; }
                                else
                                {
                                    this.AntiDiagPoint--;

                                    if (this.AntiDiagPoint == -3) {
                                        End(); 
                                        return (int)GridValue.PLAYER_TWO; }
                                }
                            }

                            if (this.DiagPoint != 5)
                            {
                                if (this.DiagPoint < 0)
                                {
                                    this.DiagPoint = 5;
                                }
                                else
                                {
                                    this.DiagPoint--;

                                    if (this.DiagPoint == -3) {
                                        End();
                                        return (int)GridValue.PLAYER_TWO; }
                                }
                            }
                        }

                        //antidiag
                        if (x + y == 2)
                        {
                            if (this.AntiDiagPoint <= 0)
                            {
                                this.AntiDiagPoint--;
                                if (this.AntiDiagPoint == -3) {
                                    End();
                                    return (int)GridValue.PLAYER_TWO; }
                            }
                            else
                            {
                                this.AntiDiagPoint = 5;
                            }
                        }
                    }


                    return 0;
                }

                return -1;


            }



            return -1;


        }



        public override void Start()
        {
            this.State = Base.Model.Enums.GameState.PLAYING;
        }

        public override void End()
        {
            this.State = Base.Model.Enums.GameState.OVER;
            this.EndTime = DateTime.Now;
        }
    }
}
