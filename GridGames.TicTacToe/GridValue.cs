﻿using System;
using System.Collections.Generic;
using System.Text;

namespace GridGames.TicTacToe
{
    public enum GridValue
    {
        EMPTY = 0,
        PLAYER_ONE = 1,
        PLAYER_TWO = 2
    }
}
